using JobNimbusCodeChallenge.MultipleSums;
using JobNimbusCodeChallenge.Parser;
using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace JobNimbusCodeChallenge.Test
{
    public class MultipleSumsTests
    {
        private readonly IMultipleSumService _multipleSumService;

        public MultipleSumsTests()
        {
            _multipleSumService = new MultipleSumService();
        }

        [Test]
        public void ValidResultMultiples3And5Input10()
        {
            var multiplesInput = new List<int>() { 3, 5 };
            var input = 10;
            var correctAnswer = 23;

            var resultAnswer = _multipleSumService.SumMultiples(multiplesInput, input);

            Assert.AreEqual(correctAnswer, resultAnswer);
        }

        [Test]
        public void ValidResultMultiples3And5Input1000()
        {
            var multiplesInput = new List<int>() { 3, 5 };
            var input = 1000;
            var correctAnswer = 266333;

            var resultAnswer = _multipleSumService.SumMultiples(multiplesInput, input);

            Assert.AreEqual(correctAnswer, resultAnswer);
        }
    }
}