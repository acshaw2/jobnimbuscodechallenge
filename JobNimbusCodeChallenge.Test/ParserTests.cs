using JobNimbusCodeChallenge.Parser;
using NUnit.Framework;

namespace JobNimbusCodeChallenge.Test
{
    public class ParserTests
    {
        private readonly IParserService _parserService;

        public ParserTests()
        {
            _parserService = new ParserService();
        }

        [Test]
        [TestCase("{}")]
        [TestCase("")]
        [TestCase("{abc...xyz}")]
        public void ValidBracketsTest(string input)
        {
            int errorAt;
            var response = _parserService.ParseBrackets(input, out errorAt);

            Assert.IsTrue(response);
            Assert.AreEqual(-1, errorAt);
        }

        [Test]
        [TestCase("}{")]
        [TestCase("{{}")]
        public void InvalidBracketsTest(string input)
        {
            int errorAt;
            var response = _parserService.ParseBrackets(input, out errorAt);

            Assert.IsFalse(response);
            Assert.AreNotEqual(-1, errorAt);
        }
    }
}