﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JobNimbusCodeChallenge.MultipleSums
{
    public class MultipleSumService : IMultipleSumService
    {
        public int SumMultiples(List<int> multiples, int input)
        {
            int sum = 0;
            input--;

            foreach (var multiple in multiples)
            {
                int multipleCount = input / multiple;

                int subtotal = multipleCount * (multipleCount + 1) / 2;

                sum += subtotal * multiple;
            }

            return sum;
        }
    }
}
