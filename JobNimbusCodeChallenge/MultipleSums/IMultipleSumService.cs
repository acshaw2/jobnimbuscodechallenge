﻿using System.Collections.Generic;

namespace JobNimbusCodeChallenge.MultipleSums
{
    public interface IMultipleSumService
    {
        int SumMultiples(List<int> multiples, int input);
    }
}