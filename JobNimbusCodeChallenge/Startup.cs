﻿using JobNimbusCodeChallenge.MultipleSums;
using JobNimbusCodeChallenge.Parser;
using Microsoft.Extensions.DependencyInjection;

namespace JobNimbusCodeChallenge
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddTransient<IParserService, ParserService>();
            services.AddTransient<IMultipleSumService, MultipleSumService>();
            services.AddTransient<JobNimbusDemo>();
        }
    }
}
