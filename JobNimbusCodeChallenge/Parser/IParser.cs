﻿namespace JobNimbusCodeChallenge.Parser
{
    public interface IParserService
    {
        bool ParseBrackets(string input, out int errorAt);
    }
}