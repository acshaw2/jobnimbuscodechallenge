﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JobNimbusCodeChallenge.Parser
{
    public class ParserService : IParserService
    {
        const char LeftBracket = '{';
        const char RightBracket = '}';

        public bool ParseBrackets(string input, out int errorAt)
        {
            var items = new Stack<int>(input.Length);

            errorAt = -1;
            for (int i = 0; i < input.Length; i++)
            {
                char c = input[i];
                if (c == LeftBracket)
                    items.Push(i);
                else if (c == RightBracket)
                {
                    if (items.Count == 0)
                    {
                        errorAt = i + 1;
                        return false;
                    }
                    items.Pop();
                }
            }
            if (items.Count > 0)
            {
                errorAt = items.Peek() + 1;
                return false;
            }
            return true;
        }

    }
}
