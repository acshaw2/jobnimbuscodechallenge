﻿using JobNimbusCodeChallenge.MultipleSums;
using JobNimbusCodeChallenge.Parser;
using System;
using System.Collections.Generic;
using System.Text;

namespace JobNimbusCodeChallenge
{
    public class JobNimbusDemo
    {
        private readonly IParserService _parserService;
        private readonly IMultipleSumService _multipleSumService;

        public JobNimbusDemo(IParserService parserService, IMultipleSumService multipleSumService)
        {
            _parserService = parserService;
            _multipleSumService = multipleSumService;
        }

        public void RunDemo()
        {
            var menu = DisplayMenu();

            while (menu != "3")
            {
                if (menu == "1")
                    this.RunParser();
                if (menu == "2")
                    this.RunSumOfMultiples();

                menu = DisplayMenu();
            }
        }

        private string DisplayMenu()
        {
            string menu;

            Console.WriteLine($"\nPress 1 to test brackets.");
            Console.WriteLine($"Press 2 to test multiples of 3 or 5 sums");
            Console.WriteLine($"Press 3 to exit");
            menu = Console.ReadLine();
            return menu;
        }

        private void RunParser()
        {
            int errorAt;

            Console.WriteLine("Enter a test string:");
            string input = Console.ReadLine();
            var isValidInput = _parserService.ParseBrackets(input, out errorAt);
            Console.WriteLine(isValidInput ? $"{input} is a valid input." : $"{input} is an invalid input with an error on character {errorAt}");
        }

        private void RunSumOfMultiples()
        {
            Console.WriteLine("This will display the sum of all multiples 3 and 5 below a given input.");
            Console.WriteLine("Enter an input:");
            int input = Convert.ToInt32(Console.ReadLine());
            var sum = _multipleSumService.SumMultiples(new List<int>() { 3, 5 }, input);
            Console.WriteLine($"The sum of all multiples of 3 and 5 below the input of {input} is {sum}.");
        }
    }
}
