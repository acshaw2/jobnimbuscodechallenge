﻿using JobNimbusCodeChallenge.Parser;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace JobNimbusCodeChallenge
{
    public class Program
    {
        static void Main()
        {
            IServiceCollection services = new ServiceCollection();

            Startup startup = new Startup();
            startup.ConfigureServices(services);

            IServiceProvider serviceProvider = services.BuildServiceProvider();

            serviceProvider.GetService<JobNimbusDemo>().RunDemo();
        }
    }
}
